// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"
#include "Interactable.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementPadding = 100.f;
	MovementSpeed = 1.f;
	LastMovementDirection = EMovementDirection::DOWN;
	CurrentMovementDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	ResetSpeed();
	AddSnakeElement(4, true);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	//BonusTick();
}

void ASnakeBase::AddSnakeElement(int ElementsNum, bool bIsBeginPlay)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		// i �� ��������� � ���������, ������ ��� �� ���������� ���������� ��������� �������,
		// ������� �� ����������� � ������ �������� ����� � ������� Add.
		FTransform NewLocation;
		if (bIsBeginPlay)
		{
			FVector NewPadding(SnakeElementArray.Num() * ElementPadding, 0, 0);
			NewLocation = FTransform(NewPadding+GetActorLocation());
		}
		else
		{
			PlayerOwner->CreateFood();
			NewLocation = FTransform(SnakeElementArray[SnakeElementArray.Num() - 1]->GetActorLocation());

		}
			
		ASnakeElementBase* SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewLocation);
		//SnakeElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		SnakeElement->SnakeOwner = this;
		int32 ElemIndex = SnakeElementArray.Add(SnakeElement);
		if (ElemIndex == 0)
		{
			SnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (CurrentMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementPadding;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementPadding;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementPadding;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementPadding;
		break;
	default:
		break;
	}

	LastMovementDirection = CurrentMovementDirection;

	SnakeElementArray[0]->ToggleCollision();
	for (int i = SnakeElementArray.Num()-1; i > 0; i--)
	{
		auto CurrentElement = SnakeElementArray[i];
		auto PrevElement = SnakeElementArray[i - 1];
		CurrentElement->SetActorLocation(PrevElement->GetActorLocation());
	}
	
	SnakeElementArray[0]->AddActorWorldOffset(MovementVector);
	SnakeElementArray[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex = SnakeElementArray.Find(OverlappedElement);
		bool bIsHead = (ElemIndex == 0);

		if (IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor))
		{
			InteractableInterface->Interact(this, bIsHead);
		}
	}
}

void ASnakeBase::ResetSpeed()
{
	SetActorTickInterval(1/MovementSpeed);
}

void ASnakeBase::SpeedChange(float Value)
{
	MovementSpeed+=Value;
	if (MovementSpeed<0.1)
	{
		MovementSpeed = 0.1;
	}
	ResetSpeed();
}

void ASnakeBase::ShiftHeadFor(FVector Offset)
{
	SnakeElementArray[0]->AddActorWorldOffset(Offset);
}

//void ASnakeBase::BonusTick()
//{
//	for (int i = ActiveBonus.size() - 1; i >= 0; i--)
//	{
//		if (!(ActiveBonus[i]->tick()))
//		{
//			delete ActiveBonus[i];
//			ActiveBonus.erase(ActiveBonus.begin()+i);
//		};
//	}
//}

