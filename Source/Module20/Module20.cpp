// Copyright Epic Games, Inc. All Rights Reserved.

#include "Module20.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Module20, "Module20" );
