// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "InputActionValue.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
class ASpeedBonus;
class UInputMappingContext;
class UInputAction;

UCLASS()
class MODULE20_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	TEnumAsByte<ECameraProjectionMode::Type> PrjoectionMode;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float OrthoWidth;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	FVector2D TopLeftCoordinate;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	FVector2D BottomRidhtCoordinate;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	TSubclassOf<ASpeedBonus> SpeedUpBonusActorClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	TSubclassOf<ASpeedBonus> SpeedDownBonusActorClass;

	UPROPERTY(EditDefaultsOnly)
	FVector SnakeBeginPoisition;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Input thing
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputMappingContext* InputMapping;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputAction* VerticalMoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputAction* HorizontalMoveAction;
		

	void MoveVertical(const FInputActionValue& Value);
	void MoveHorizontal(const FInputActionValue& Value);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	FVector GetRandomLocationOnField();

	void CreateFood();

	void CreateSpeedUpBonus();

	void CreateSpeedDownBonus();
};
