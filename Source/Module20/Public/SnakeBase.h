// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <vector>
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class APlayerPawnBase;

//class BonusBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class MODULE20_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementPadding;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElementArray;

	UPROPERTY()
	EMovementDirection LastMovementDirection;

	UPROPERTY()
	EMovementDirection CurrentMovementDirection;

	UPROPERTY()
	APlayerPawnBase* PlayerOwner;

	//std::vector<BonusBase*> ActiveBonus;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1, bool bIsBeginPlay = false);

	UFUNCTION(BlueprintCallable)
	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor);
	
	//UFUNCTION()
	//void BonusTick();

	UFUNCTION()
	void ResetSpeed();

	UFUNCTION()
	void SpeedChange(float Value = 1.f);

	UFUNCTION()
	void ShiftHeadFor(FVector Offset);
	
};

//class BonusBase
//{
//	int timer;
//protected:
//
//	ASnakeBase* SnakeOwner;
//public:
//	BonusBase(ASnakeBase* Snake, int time) : SnakeOwner(Snake), timer(time)
//	{
//	};
//	virtual ~BonusBase()
//	{
//	};
//	virtual int tick()
//	{
//		return --timer;
//	}
//};
//
//class SpeedUp : public BonusBase
//{
//public:
//	SpeedUp(ASnakeBase* Snake, int time) : BonusBase(Snake, time)
//	{
//		SnakeOwner->MovementSpeed -= 0.15;
//		SnakeOwner->SetActorTickInterval(SnakeOwner->MovementSpeed);
//	}
//	~SpeedUp()
//	{
//		SnakeOwner->MovementSpeed += 0.15;
//		SnakeOwner->SetActorTickInterval(SnakeOwner->MovementSpeed);
//	}
//};